call aglio -i Doc_API_12-08-21_v1.apib -o new_api_v4_pre.html --theme-full-width --theme-variables flatly
call echo -GIT add-
call python replace_title.py
call git add new_api_v4.html
call echo -Commit GIT-
call git commit -m "Automatic commit documentation change `date +\"%Y-%m-%d\"`"
git config --global credential.helper wincred
call echo -GIT push-
call git push origin master
call echo -exit-
exit