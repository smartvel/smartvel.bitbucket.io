import re


with open("new_api_v4_pre.html", "r", encoding='utf-8') as file_input:
    with open("new_api_v4.html", "w", encoding='utf-8') as output: 
        for line in file_input:
            if "<title>" in line:
                output.write(re.sub('<title>?(.*?)</title>', '<title>SMARTVEL ENTRY RESTRICTIONS API V4</title>', line,  flags=re.DOTALL))             
            else:
                output.write(line)

            
            
    